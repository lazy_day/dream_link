import { Hero } from './hero';

export const HEROES: Hero[] = [
  { id: 11, name: 'Inventory' },
  { id: 12, name: 'Accounting' },
  { id: 13, name: 'Production' },
  { id: 14, name: 'Hr' }
];
